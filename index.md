# Understanding Natural Language Processing with Me! 

\blurb{
      Hi, I’m Shiyi. Welcome to my technical blog. I will be documenting my journey of learning Natural Language Processing here. I will be presenting everything I have learned so far, including important concepts, necessary code snippets, and more. I am by no means an expert in this subject, but I have gone through extensive studies and training in the fields and subfields related to have had a good grasp of what’s important. 
}


Areas that I have dabbled and focused in, 
```
→ General Linguistics
→ Socioliguistics
→ Symbolic Computational Linguistics
→ Statistical Natural Language Processing
→ State of the Art Language Modeling Tasks Benchmarking
```

---
###
### **The Subject Matter**
###
What do we mean by Natural Language Processing? 
    if we do a little googling and researching, it's very intuitive that natural language processing involves a set of solutions to various natural human language tasks. The most common ones are
```
        
→ Sentiment analysis
→ Machine translation 
→ Word-sense disambiguation 
→ Named-entity recognition 
→ Topic modeling 
→ Document classification 
→ Question answering 
```
---
### 
### **A Little Bit Of History** 

The history of Computational Linguistics dates back to the 40s to 50s. So, it's not very long ago that the field that has created ChatGPT or any form of AI that is so commonly adopted in every aspect of out lives started to have its very first ancestorial ideation. It's still a fairly new and young field with infinite possibilities up for exploration. 

Before diving in, first we have to ask ourselves what exactly is artificial intelligence (AI)?

Well, according to the official definition extracted out of John McCarthy's 2004 [paper](https://www-formal.stanford.edu/jmc/whatisai.pdf) listed on IBM's [website](https://www.ibm.com/topics/artificial-intelligence),

```
🤖️ "It is the science and engineering of making intelligent machines, especially intelligent 
computer programs. It is related to the similar task of using computers to understand 
human intelligence, but AI does not have to confine itself to methods that are biologically 
observable."
```

So if it's to understand human intelligence, we need to know humans gain information and how human intelligence, or the brain, really works both through physiology and psychology, 

```
💡 Two Important Sources of Knowledge: Rationalism and Empiricism. 
The first acquires knowledge through reasoning and logic, while the second through experience 
and experimentation.
```

Below are some important notes about the **historical timeline**:


\textoutput{cards}

---
### 
### Topics We Care About
- [Topic 0: Information Theory](./modules/info-theory)
    - [Subtopic 0: Noisy Channel Model](./modules/noisy-channel-model) 
    - [Subtopic 1: Cryptography](./modules/cryptography)
    - [Subtopic 2: Mutual Information](./modules/mutual-info)
    - [Subtopic 3: Information Retrieval](./modules/info-ret)
- [Topic 1: Math and Physics](./modules/general-overview)
    - [Subtopic 0: Calculus](./modules/calculus)
    - [Subtopic 1: Linear Algebra](./modules/linear-alg)
    - [Subtopic 2: Formal Logic](./modules/logic)
    - [Subtopic 3: Statistics and Probability](./modules/stat-prob)
    - [Subtopic 4: Discrete Math](./modules/disrete-math)
- [Topic 2a: Computationalism](./modules/symbolic-language-processing)
    - [Subtpoic 0: Chomsky Hierarchy](./modules/chomsky-hierarchy)
    - [Subtopic 1: Context Free Grammar](./modules/context-free-grammar)
    - [Subtopic 2: Finite State Automata](./modules/finite-stat-automata)
    - [Subtopic 3: Linguistic Trees](./modules/linguistic-trees)
    - [Subtopic 4: Parsing](./modules/parsing)
    - [Subtopic 5: Hidden Markov Model](./modules/hidden-markov-model)
    - [Subtopic 6: Word Net](./modules/word-net)
    - [Subtopic 7: Universal Dependencies](./modules/universal-dependencies)
- [Topic 2b: Connectionism](./modules/modern_nlp)
    - [Subtopic 2a: Pytorch Tensors](./modules/2a-pytorch-tensors)
    - [Subtopic 2b: Automatic Differentiation](./modules/2b-automatic-differentiation)
    - [Subtopic 3: Loss functions for classification](./modules/3-loss-functions-for-classification)
    - [Subtopic 4: Optimization for Deep Learning](./modules/4-optimization-for-deep-learning)
    - [Subtopic 5: Stacking layers](./modules/5-stacking-layers)
    - [Subtopic 6: Convolutional Neural Network](./modules/6-convolutional-neural-network)
    - [Subtopic 7a: Embedding layers and dataloaders](./modules/7a-embedding-layers-dataloaders)
    - [Subtopic 7b: Collaborative Filtering](./modules/7b-collaborative-filtering)
    - [Modules 8: Autoencoders](./modules/8-autoencoders)
    - [Subtopic 9: Generative Adversarial Networks](./modules/9-generative-adversarial-networks)
    - [Subtopic 10a: Recurrent Neural Networks theory](./modules/10a-recurrent-neural-networks-theory)
    - [Subtopic 10b: Recurrent Neural Networks practice](./modules/10b-recurrent-neural-networks-practice)

